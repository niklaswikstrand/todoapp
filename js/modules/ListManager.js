TodoApp.ListManager = function(selector) {
    
    this._name          = 'ListManager';
    this._selector      = selector;
    this._url           = 'http://tododata.nicknamed.se/';
    return this;
}

TodoApp.ListManager.prototype.activate = function() {
    if(this._selector) {
        
        this.registerListeners();
        
        // Do initial request for todos´s
        this.getRequest();
    }
};

TodoApp.ListManager.prototype.registerListeners = function() {
    
    var _that = this;
    
    // Listen for form submit
    $(this._selector).find('form').submit(function(event) {
        
        event.preventDefault();
        
        if($(this).find('input').val() != "") {
            
            var formdata    = $(this).serializeObject();
            
            _that.postRequest(formdata);
            
            $(this)[0].reset();
        }

    });

    // Listen for checkbox change
    $(this._selector).on('change', 'li input[type=checkbox].done-switch', function(event) {

        event.preventDefault();
        
        // Build data object
        var data = {
            id: $(this).parents('li').data('id'),
            val: $(this).is(':checked')
        }

        // Run put request
        _that.putRequest(data);

    });


    // Listen for button click
    $(this._selector).on('click', 'li button.delete', function(event) {

        event.preventDefault();

        var data = {
            id: $(this).parents('li').data('id'),
            val: $(this).is(':checked')
        }

        _that.deleteRequest(data);

    });

};

TodoApp.ListManager.prototype.getRequest = function() {
    
    var _that = this;

    $.ajax({
        url: _that._url,
        type: 'GET',
        beforeSend: function(data){

            // Render Loading Container before async call
            _that.renderLoadingIcon('.todoapp-items');

        },
        success: function(data){

            // Render List Element based on returned data
            _that.renderListElement(data);

            // Fadeout and remove any occuring loading-containers
            _that.removeLoadingIcon();

            // If error is present, remove
            _that.removeError();

        },
        error: function(){
            
            _that.renderError({ text: 'Could not get data from server...' }, '.todoapp-items');

            // Fadeout and remove any occuring loading-containers
            _that.removeLoadingIcon();
        }
    });

}

TodoApp.ListManager.prototype.postRequest = function(data) {
     
    var _that = this;

    $.ajax({
        url: _that._url,
        type: 'POST',
        data: data,
        beforeSend: function(){
            
            // Render Loading Container before async call
            _that.renderLoadingIcon('.inner-container');

        },
        success: function(id) {
            
            // Extend data with returned ID
            $.extend(data, {
                id: id
            });

            // Render List element based on data
            _that.renderListElement([data]);

            // Fadeout and remove any occuring loading-containers
            _that.removeLoadingIcon();

            // If error is present, remove
            _that.removeError();

        },
        error: function(){

            _that.renderError({ text: 'Error while trying to send data to server...' }, '.todoapp-items');

            // Fadeout and remove any occuring loading-containers
            _that.removeLoadingIcon();
        }

    });

}

TodoApp.ListManager.prototype.putRequest = function(data, before, callback) {
    
    var _that = this;

    $.ajax({
        url: _that._url + data.id,
        type: 'PUT',
        data: data,
        beforeSend: function() {
            _that.renderLoadingIcon('#item_' + data.id);
        },
        success: function(item){
            
            // Update counter
            _that.renderCounter('.todo-counter');

            // Fadeout and remove any occuring loading-containers
            _that.removeLoadingIcon();

            // Unfocus all fields that might be in focus at success
            $('input').blur();

            // If error is present, remove
            _that.removeError();

        },
        error: function() {
            
            _that.renderError({ text: 'Error while trying to update item...' }, '.todoapp-items');

            // Fadeout and remove any occuring loading-containers
            _that.removeLoadingIcon();

            $('#item_' + data.id).find('.done-switch').prop('checked', (data.val ? false : true));

        }

    });
}

TodoApp.ListManager.prototype.deleteRequest = function(data) {
    
    var _that = this;
    
    $.ajax({
        url: _that._url + data.id,
        type: 'DELETE',
        data: data,
        beforeSend: function() {
            _that.renderLoadingIcon('#item_' + data.id);
        },
        success: function(item){
            
            $('#item_' + data.id).hide(200, function(){
                $(this).remove();
                // Update counter
                _that.renderCounter('.todo-counter');
            });

            $('input').blur();

            // If error is present, remove
            _that.removeError();

        },
        error: function() {
            
            _that.renderError({ text: 'Error while trying to delete item...' }, '.todoapp-items');

            // Fadeout and remove any occuring loading-containers
            _that.removeLoadingIcon();

        }
    });
}

TodoApp.ListManager.prototype.renderListElement = function(data) {
    
    var _that = this;
    
    // Get mustasch template
    $.get('/templates/todo-item.mst', function(template) {
        
        $.each(data, function(index, value) {
            var rendered = Mustache.render(template, value);
            $(_that._selector).find('ul').prepend(rendered)
        });

        // Render Counter
        _that.renderCounter('.todo-counter');

    });
}

TodoApp.ListManager.prototype.renderLoadingIcon = function(targetElement){
    $.get('/templates/todo-loading.mst', function(template) {
        $(targetElement).append(Mustache.render(template));
    });
}

TodoApp.ListManager.prototype.removeLoadingIcon = function(targetElement){
    if($('.loading-container').length > 0) {
        $('.loading-container').fadeOut(200, function(){
            $(this).remove();
        });
    }
}

TodoApp.ListManager.prototype.renderCounter = function(targetElement){
    
    var _that = this;
    
    var data = {
        totalCount: $(_that._selector).find('li').length,
        doneCount: $(_that._selector).find('li .done-switch:checked').length
    }

    $.get('/templates/todo-counter.mst', function(template) {
        $(targetElement).html(Mustache.render(template, data));
    });
}

TodoApp.ListManager.prototype.renderError = function(data, targetElement){
    
    this.removeLoadingIcon();

    $.get('/templates/todo-error.mst', function(template) {
        $(targetElement).prepend(Mustache.render(template, data));
    });
}
TodoApp.ListManager.prototype.removeError = function(targetElement){
    if($('.error-container').length > 0) {
        $('.error-container').remove();
    }
}